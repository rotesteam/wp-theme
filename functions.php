<?php

add_action( 'wp_enqueue_scripts', 'rtwp_theme_assets' );
function rtwp_theme_assets() {
    $modificated = date( 'YmdHis', filemtime( get_stylesheet_directory() . '/assets/css/rtwp_styles.min.css' ) );
    wp_register_script( 'bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.bundle.min.js' );
    wp_register_script( 'scroll', get_stylesheet_directory_uri() . '/assets/js/scroll.js' );
    wp_register_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_register_style( 'rtwp_styles', get_stylesheet_directory_uri() . '/assets/css/rtwp_styles.min.css',array(),$modificated);
    add_editor_style( 'assets/css/bootstrap.min.css' );
    wp_enqueue_script( 'scroll' );
    wp_enqueue_script( 'bootstrap' );
    wp_enqueue_style( 'bootstrap' );
    wp_enqueue_style( 'rtwp_styles' );
    wp_register_style( 'main', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_style( 'main' );
}

function rtwp_gutenberg_css(){
    add_theme_support( 'bootstrap-styles' );
    add_editor_style( 'assets/css/bootstrap.min.css' );
    add_editor_style( 'assets/css/rtwp_styles.min.css' );


}
add_action( 'after_setup_theme', 'rtwp_gutenberg_css', 8 );


include_once(get_stylesheet_directory() . '/assets/blocks/rtwp_blocks.php');

function rtwp_admin_css(){
    wp_register_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_register_script( 'bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.bundle.min.js' );
    wp_enqueue_style( 'bootstrap' );
    wp_enqueue_script( 'bootstrap' );
}
add_action('admin_enqueue_scripts', 'rtwp_admin_css');



