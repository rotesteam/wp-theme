<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;

$className = $block['className'];
$style= $block['style'];
$align= ' align' . $block['align'];

echo '<InnerBlocks class="rtwp-container d-flex flex-wrap '. $className . $align .'" />';

?>