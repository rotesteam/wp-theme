<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'init', 'register_acf_blocks' );

function register_acf_blocks() {
    register_block_type( get_stylesheet_directory() . '/assets/blocks/rtwp_tilecontainer/block.json' );
    register_block_type( get_stylesheet_directory() . '/assets/blocks/rtwp_tiletopic/block.json' );
    register_block_type( get_stylesheet_directory() . '/assets/blocks/rtwp_tilequote/block.json' );
    register_block_type( get_stylesheet_directory() . '/assets/blocks/rtwp_tileimage/block.json' );
    register_block_type( get_stylesheet_directory() . '/assets/blocks/test/block.json' );
}

add_action( 'init', 'register_acf_fields' );

function register_acf_fields(){
    include_once(get_stylesheet_directory() . '/assets/blocks/rtwp_tiletopic/rtwp_tiletopic_acf.php');
    include_once(get_stylesheet_directory() . '/assets/blocks/rtwp_tilequote/rtwp_tilequote_acf.php');
    include_once(get_stylesheet_directory() . '/assets/blocks/rtwp_tileimage/rtwp_tileimage_acf.php');

}