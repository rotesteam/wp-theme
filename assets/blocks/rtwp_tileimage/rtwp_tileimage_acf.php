<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;


if( function_exists('acf_add_local_field_group') ):

	$tileimage_field_image[]=array(//rtwp_tileimage_image
		'key' => 'field_rtwp_tileimage_image',
		'label' => 'Bild',
		'name' => 'rtwp_tileimage_image',
		'type' => 'image',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
        'return_format' => 'id',
    );
	
	$tileimage_block_fields=array_merge(
		$tileimage_field_image,
	);

acf_add_local_field_group(array(
	'key' => 'group_rtwp_tileimage_block',
	'title' => '',
	'fields' => $tileimage_block_fields,
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'rtwp/tileimage',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;