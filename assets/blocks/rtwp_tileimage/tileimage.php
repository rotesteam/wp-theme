<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;




// Create id attribute allowing for custom "anchor" value.
$id = 'rtwp-block-tileimage-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'rtwp-block-tileimage';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}



// load the fields
$image = get_field('rtwp_tileimage_image');
$imagediv = '';
if ($image) {
    $imageurl = wp_get_attachment_image_src($image, 'large');
    $imagediv = ' style=" background-image: url(' . esc_url($imageurl[0]) . ') "';
}

// Block

$out = '<div id="'.$id. '" class="'. $className .' col-12 col-md-6 col-xxl-4">';
$out .= '<div class="rtwp-tile-md">';
    $out .= '<div class="image-container" '. $imagediv .'>';
        
    $out .= '</div>';
    $out .= rtwp_ml_over($image);
$out .= '</div>';
$out.='</div>';

echo $out;

?>