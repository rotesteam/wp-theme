<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;


if( function_exists('acf_add_local_field_group') ):

	$tilequote_fields_text[]=array(//rtwp_tilequote_text
		'key' => 'field_rtwp_tilequote_text',
		'label' => 'Zitat',
		'name' => 'rtwp_tilequote_text',
		'type' => 'textarea',
		'instructions' => 'Für optimale Darstellung maximal ca. 100 Zeichen inkl. Leerzeichen',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '100',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier das Zitat. Möglichst max. 80 Zeichen',
	);

	$tilequote_fields_author[]=array(//rtwp_tilequote_author
		'key' => 'field_rtwp_tilequote_author',
		'label' => 'Autor',
		'name' => 'rtwp_tilequote_author',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier die Autorin',
	);
	$tilequote_fields_function[]=array(//rtwp_tilequote_function
		'key' => 'field_rtwp_tilequote_function',
		'label' => 'Funktion',
		'name' => 'rtwp_tilequote_function',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier die Autorin',
	);
	$tilequote_field_image[]=array(//rtwp_tilequote_image
		'key' => 'field_rtwp_tilequote_image',
		'label' => 'Vorschaubild',
		'name' => 'rtwp_tilequote_image',
		'type' => 'image',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
        'return_format' => 'id',
		'placeholder' => 'Abweichende Verlinkung auswählen',
    );
	$tilequote_fields_database[]=array(//rtwp_tilequote_database
		'key' => 'field_rtwp_tilequote_database',
		'label' => 'Zitat aus der Datenbank',
		'name' => 'rtwp_tilequote_database',
		'type' => 'true_false',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
		  'width' => '30',
		  'class' => '',
		  'id' => '',
		),
		'message' => '',
		'default_value' => 0,
		'ui' => 1,
		'ui_on_text' => 'ja',
		'ui_off_text' => 'nein',
	);
	
	

	$tilequote_block_fields=array_merge(
		$tilequote_fields_text,
		$tilequote_fields_author,
		$tilequote_fields_function,
		$tilequote_field_image,
	);

acf_add_local_field_group(array(
	'key' => 'group_rtwp_tilequote_block',
	'title' => '',
	'fields' => $tilequote_block_fields,
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'rtwp/tilequote',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;