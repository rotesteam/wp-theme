<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;




// Create id attribute allowing for custom "anchor" value.
$id = 'rtwp-block-tilequote-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'rtwp-block-tilequote';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}



// load the fields
$author = get_field( 'rtwp_tilequote_author' );
$function = get_field( 'rtwp_tilequote_function' );
$quote = get_field( 'rtwp_tilequote_text' );
$image = get_field('rtwp_tilequote_image');
$imagediv = '';
if ($image) {
    $imageurl ='';
    $imageurl = wp_get_attachment_image_src($image, 'medium');
    $imagediv = ' style=" background-image: url(' . esc_url($imageurl[0]) . ') "';
}

// Block

$out = '<div id="'.$id. '" class="'. $className .' col-12 col-md-6 col-xxl-4">';
$out .= '<div class="rtwp-tile-md">';
    $out .= '<div class="quote-container" '. $imagediv .'>';
    
        $out .= '<div class ="quote-opacity"></div>';
        
        $out.= '<div class="quote-content">'; 
            $out.= '<div class="tile-quote">' . $quote . '</div>';
            $out .='<div class="tile-author">'. $author .'</div>';
            $out .='<div class="tile-function">'. $function .'</div>';
            if ($image){
                $out .= rtwp_ml_over($image);
            }
        $out.='</div>';
    $out .= '</div>';
$out .= '</div></a>';
$out.='</div>';

echo $out;

?>