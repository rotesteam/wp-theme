<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;




// Create id attribute allowing for custom "anchor" value.
$id = 'rtwp-block-tiletopic-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'rtwp-block-tiletopic';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}



// load the fields
$headline = get_field( 'rtwp_tiletopic_headline' );
$title = get_field( 'rtwp_tiletopic_title' );
if (get_field( 'rtwp_tiletopic_urltype' )){
    $url = get_field( 'rtwp_tiletopic_url_extern' );} 
else {
    $url = get_field( 'rtwp_tiletopic_url_intern' );};

// Block

$out = '<div id="'.$id. '" class="'. $className .' col-12 col-md-6 col-xxl-4">';
$out .= '<div class="rtwp-tile-md">';
    $out .='<div class="tile-container">';
        $out.= '<div class="tile-element">'; 
            $out.= '<div class="element-pfeil-weiss"></div>';
        $out.= '</div>';
        $out.= '<div class="tile-content">'; 
            $out.='<div class="tile-headline">' . $headline . '</div>';
            $out.= '<div class="tile-title">' . $title . '</div>';
            $out.= '<div class="rtwp-button"><a href="' . $url . '" target="_blank"> mehr… </a></div>';
        $out.='</div>';
    $out.='</div>';
$out .= '</div></a>';
$out.='</div>';

echo $out;

?>