window.addEventListener('scroll', function() {
  var header = document.querySelector('header');
  var blocknavlogo = document.querySelector('.rtwp-block-navlogo');
  var headlogo = document.querySelector('.rtwp-head-logo');
  header.classList.toggle('rtwp-scroll', window.scrollY > 100);
  //blocknavlogo.classList.toggle('has-primary-background-color', window.scrollY > 100);
  blocknavlogo.classList.toggle('fixed-top', window.scrollY > 100);
  headlogo.classList.remove('w-100', window.scrollY >100);
  headlogo.classList.toggle('w-100', window.scrollY <100);
  //logo.src = window.scrollY > 50 ? 'scroll-logo.png' : 'https://spd.wpbeta.wechsel-gestalten.de/wp-content/uploads/sites/5/2023/03/SPD_AfA_RGB_Breit_ROT-768x213.png';
});